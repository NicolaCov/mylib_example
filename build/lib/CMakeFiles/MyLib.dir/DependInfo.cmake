# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/nicola/Tutorials/Test_My_Library/lib/MyLib/src/printTest.cpp" "/home/nicola/Tutorials/Test_My_Library/build/lib/CMakeFiles/MyLib.dir/MyLib/src/printTest.cpp.o"
  "/home/nicola/Tutorials/Test_My_Library/lib/MyLib/src/showLena.cpp" "/home/nicola/Tutorials/Test_My_Library/build/lib/CMakeFiles/MyLib.dir/MyLib/src/showLena.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../lib"
  "/usr/include/opencv"
  "../lib/MyLib/printTest"
  "../lib/MyLib/showLena"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
