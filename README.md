This repository contains an example on how do your own library with CMake and use it in a project.

Our library, called MyLib contains two "modules":

- printTest: which contains one function that prints something on the terminal, and another that computes the sum between two integers.
- showLena: contains just a function that load the lena.jpg (iside the bin folder) and display it (with OpenCV)

To compile the code, first remove the build directory, recreate it and compile the code:

```
#!

$ rm -r build
$ mkdir build
$ cd build
$ cmake ..
$ make 
```

Then go to the bin directory and launch the executable:

```
#!

$ ./myLibTest
```